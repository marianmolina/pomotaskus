# Pomotaskus

A personal project to practice React and have fun.
I like the idea of using a pomodoro app to better organise my working time so I thought to make my own one. It's not just a pomodoro app since you can synchronise a list of tasks to your timer.

<br/>
<br/>

## Features

### Timer

- Pomodoro timer switches automatically between working time and break time when timer finishes. The flow follows the traditional pomodoro sequence (4 times 25min work & 5min break, then 15min break and repeat)
- Play/Pause buttons
- Next/previous buttons with confirmation request if timer is running
- Application color changes according to the timer (work or break timer)
- Name of current timer running highlighted, highlight of the next timer name when hover "Next" button.

### Task

- Add, update and delete your tasks
- Set the number of pomodoro you evaluate each task will take
- See how many pomodoros you already finished for each task
- Change the order of your tasks either with the arrows directly on the task or inside the update menu with the target index
- Get an estimation of the time you'll be finished with all your tasks
- Automatic scrolling to the current task
- Automatic scrolling to last task when add new tasks

### Timer synchronised to the tasks

- See timer and current task (or break) on the tab title
- Current task highlighed on the tasks list
- Increment number of finished pomodoros for the current task when timer finishes
- When all the pomodoros of one task are finished, the next task in the list becomes the current
- The current task always gonna be the first one on the list that doesn't have all its pomodoros validated

### Other

- Prevent your screen from locking to allow the timer to keep running (not available in all browsers)
- On break times a menu displays :
  - a prevention message to encourage users to take an active break
  - an updatable list of ideas for their break

<br/>
<br/>

## Coming later

- Animations
- Add offline
- Add screen notification when timer ends

<br/>
<br/>

## Credit

- UI inspired from : https://pomofocus.io/app
- Ring sound : https://mixkit.co/free-sound-effects/nature/

## Licence

This work is licensed under a Creative Commons Attribution 4.0 International License.
http://creativecommons.org/licenses/by/4.0/
