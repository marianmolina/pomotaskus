const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Lucida Sans", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        pomodoro: "#DD804C",
        pomodoromd: "#BB754D",
        pomodorodark: "#905F43",

        shortbreak: "#689D83",
        shortbreakmd: "#4E7C65",
        shortbreakdark: "#355C49",

        longbreak: "#4C9195",
        longbreakmd: "#4B7B7D",
        longbreakdark: "#345A5C",
      },
      screens: {
        xxxs: "350px",
        xxs: "400px",
        xs: "540px",
      },
    },
  },
  plugins: [],
};
