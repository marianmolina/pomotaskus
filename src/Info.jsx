function Info({ TIMES, index, time, tasks }) {
  const timeNow = new Date();

  // TIME LEFT BEFORE END OF TASKS/POMODOROS SET
  const indexPomo = Math.floor(index / 2);

  const sumPomodoroLeft = tasks.reduce((previousValue, currentValue) => {
    return previousValue + currentValue.nbTotal - currentValue.nbCurrent;
  }, 0);

  const nbCycleLeft = Math.floor(sumPomodoroLeft / 5);
  const remainingPomo = sumPomodoroLeft % 5;

  const sumShortBreaksLeft =
    indexPomo === 0
      ? nbCycleLeft * 4 + remainingPomo - 1
      : nbCycleLeft * 3 + remainingPomo;

  const sumLongBreaksLeft = remainingPomo
    ? indexPomo + remainingPomo > 5
      ? nbCycleLeft + 1
      : nbCycleLeft
    : indexPomo === 0
    ? nbCycleLeft - 1
    : nbCycleLeft;

  const timeLeft =
    sumPomodoroLeft * 25 * 60000 +
    sumShortBreaksLeft * 5 * 60000 +
    sumLongBreaksLeft * 15 * 60000;

  const timePassed = TIMES[index] * 60000 - (time / 60) * 60000;

  const timeEnd =
    // If a timer runs
    (TIMES[index] === 25 && time !== 25 * 60) ||
    (TIMES[index] === 5 && time !== 5 * 60) ||
    (TIMES[index] === 15 && time !== 15 * 60)
      ? new Date(timeNow.getTime() + timeLeft - timePassed)
      : new Date(timeNow.getTime() + timeLeft);

  const minutesEnd =
    timeEnd.getMinutes().toString().length === 1
      ? "0" + timeEnd.getMinutes()
      : timeEnd.getMinutes();
  const hoursEnd =
    timeEnd.getHours().toString().length === 1
      ? "0" + timeEnd.getHours()
      : timeEnd.getHours();

  return (
    <div className="mb-2 sm:ml-auto sm:mb-0">
      <p className="text-white/70">
        {sumPomodoroLeft === 0 ? (
          "No task left."
        ) : (
          <span>
            Finish at{" "}
            <span className="text-lg font-bold text-white">
              {hoursEnd}:{minutesEnd}
            </span>
          </span>
        )}
      </p>
    </div>
  );
}

export default Info;
