import { useState, useEffect, useRef } from "react";
import Message from "./Message";

const POMODORO = {
  25: "Pomodoro",
  5: "Short Break",
  30: "Long Break",
};

const playAudio = (url) => {
  const audio = new Audio(url);
  audio.play();
};

function Timer({
  TIMES,
  time,
  updateTime,
  running,
  setRunning,
  tasks,
  index,
  updateIndex,
}) {
  const [nextHovered, setNextHovered] = useState(false);
  const [previousHovered, setPreviousHovered] = useState(false);
  const [displayMessage, setDisplayMessage] = useState(false);

  const minutes =
    Math.floor(time / 60).toString().length === 1
      ? "0" + Math.floor(time / 60)
      : Math.floor(time / 60);
  const seconds =
    (time % 60).toString().length === 1 ? "0" + (time % 60) : time % 60;
  const lastIndex = index == TIMES.length - 1;
  const firstIndex = index == 0;

  // Update timer on page and tab
  useEffect(() => {
    const indexCurrent = tasks.findIndex((task) => task.current === true);

    document.title = `${minutes}:${seconds} - ${
      TIMES[index] === 25
        ? indexCurrent < 0
          ? POMODORO[TIMES[index]]
          : tasks[indexCurrent].name
        : POMODORO[TIMES[index]]
    }`;

    if (time > 0 && running) {
      const timer = setInterval(() => {
        updateTime(time - 1);
      }, 1000);
      return () => clearInterval(timer);
    } else if (time === 0) {
      playAudio("/mixkit-thunder-rumble-during-a-storm-2395.wav");
      updateIndex(index + 1);
      index == TIMES.length - 1 ? updateIndex(0) : null;
      updateTime(TIMES[index] * 60);
      if (TIMES[index] === 25) {
        setDisplayMessage(true);
      }
    }
  }, [time, running, tasks]);

  const isMounted = useRef(false);

  useEffect(() => {
    if (isMounted.current) {
      updateTime(TIMES[index] * 60);
    } else {
      isMounted.current = true;
    }
  }, [index]);

  return (
    <section
      className={
        "flex flex-col justify-around items-center p-10 mt-6 w-full rounded-md sm:p-14 bg-white/10 lg:my-4 sm:w-fit lg:min-h-[29rem] 2xl:mr-16"
      }
    >
      <div
        className={`${
          TIMES[index] === 5
            ? "text-shortbreakdark"
            : TIMES[index] === 15
            ? "text-longbreakdark"
            : "text-pomodorodark"
        }  flex justify-between w-full`}
      >
        <p
          className={`${TIMES[index] === 25 ? "text-white" : null} ${
            (TIMES[index + 1] === 25 || lastIndex) && nextHovered
              ? "text-white/60"
              : (TIMES[index - 1] === 25 || lastIndex) && previousHovered
              ? "text-white/60"
              : null
          } rounded-md flex-shrink-0`}
        >
          Pomodoro
        </p>
        <p
          className={`${TIMES[index] === 5 ? "text-white" : null} 
            ${
              TIMES[index] === 25 && index !== TIMES.length - 2 && nextHovered
                ? "text-white/60"
                : TIMES[index] === 25 && index !== 0 && previousHovered
                ? "text-white/60"
                : null
            }
            mx-2  rounded-md flex-shrink-0`}
        >
          Short Break
        </p>
        <p
          title={
            TIMES[index] === 30
              ? "Enjoy your Long Break !"
              : Math.floor(TIMES.length / 2 - index / 2) > 1
              ? `After ${Math.floor(TIMES.length / 2 - index / 2)} pomodoros`
              : `After ${Math.floor(TIMES.length / 2 - index / 2)} pomodoro`
          }
          className={`${TIMES[index] === 30 ? "text-white" : null} 
            ${
              TIMES[index] === 25 && index === TIMES.length - 2 && nextHovered
                ? "text-white/60"
                : TIMES[index] === 25 && index === 0 && previousHovered
                ? "text-white/60"
                : null
            }    rounded-md flex-shrink-0`}
        >
          Long Break
        </p>
      </div>

      <p
        className="overflow-hidden my-7 text-8xl sm:w-[360px] font-semibold sm:text-9xl"
        role="timer"
      >
        {minutes}:{seconds}
      </p>

      <div className={`flex justify-around w-full`}>
        <button
          title="switch to previous timer"
          name="previous"
          onClick={() => {
            if (running || parseInt(minutes) !== TIMES[index]) {
              const confirmBox =
                TIMES[index] === 25
                  ? window.confirm(
                      "If you quit the round before its end your timer will be lost and the current pomodoro will not be validated."
                    )
                  : window.confirm(
                      "If you quit the round early your timer will be lost."
                    );

              if (confirmBox === true) {
                setRunning(false);
                updateIndex(firstIndex ? TIMES.length - 1 : index - 1);
                updateTime(
                  firstIndex ? TIMES[TIMES.length] * 60 : TIMES[index - 1] * 60
                );
              }
            } else if (!running || parseInt(minutes) !== TIMES[index]) {
              setRunning(false);
              updateIndex(firstIndex ? TIMES.length - 1 : index - 1);
              updateTime(
                firstIndex ? TIMES[TIMES.length] * 60 : TIMES[index - 1] * 60
              );
            }
          }}
          onMouseOver={() => {
            setPreviousHovered(true);
          }}
          onMouseOut={() => {
            setPreviousHovered(false);
          }}
        >
          <svg
            width="30"
            height="38"
            viewBox="0 0 21 22"
            className="fill-white"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M19.1949 19.6046V2.25197C19.1949 1.46762 18.3331 0.988599 17.667 1.40268L3.70949 10.079C3.08007 10.4703 3.08007 11.3863 3.70949 11.7776L17.667 20.4539C18.3331 20.868 19.1949 20.3889 19.1949 19.6046Z"
              fill="inherit"
            />
            <path
              d="M2.34326 20.7759L2.34326 1.08075C2.34326 0.528465 1.89555 0.0807495 1.34326 0.0807495L1.21715 0.0807495C0.664863 0.0807494 0.217147 0.528465 0.217147 1.08075L0.217147 20.7759C0.217147 21.3282 0.664862 21.7759 1.21715 21.7759L1.34326 21.7759C1.89555 21.7759 2.34326 21.3282 2.34326 20.7759Z"
              fill="inherit"
            />
          </svg>
        </button>

        <button
          name="start/stop"
          title={running ? "Pause Timer" : "Play Timer"}
          onClick={() => setRunning(!running)}
          className="h-11"
        >
          {running ? (
            <svg
              width="36"
              height="36"
              viewBox="0 0 11 22"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.970215 20.9774V1.31488C0.970215 1.03874 1.19407 0.81488 1.47021 0.81488H2.54598C2.82213 0.81488 3.04598 1.03874 3.04598 1.31488V20.9774C3.04598 21.2536 2.82213 21.4774 2.54598 21.4774H1.47022C1.19407 21.4774 0.970215 21.2536 0.970215 20.9774ZM8.32679 20.9774V1.31488C8.32679 1.03874 8.55065 0.81488 8.82679 0.81488H9.90256C10.1787 0.81488 10.4026 1.03874 10.4026 1.31488V20.9774C10.4026 21.2536 10.1787 21.4774 9.90256 21.4774H8.82679C8.55065 21.4774 8.32679 21.2536 8.32679 20.9774Z"
                fill="white"
              />
            </svg>
          ) : (
            <svg
              width="36"
              height="44"
              viewBox="0 0 18 22"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.837891 19.6995V2.34688C0.837891 1.56253 1.6997 1.08351 2.36583 1.49759L16.3233 10.1739C16.9528 10.5652 16.9528 11.4812 16.3233 11.8725L2.36583 20.5488C1.69969 20.9629 0.837891 20.4839 0.837891 19.6995Z"
                fill="white"
                stroke="white"
              />
            </svg>
          )}
        </button>

        <button
          name="next"
          title="switch to next timer"
          onClick={() => {
            if (running || parseInt(minutes) !== TIMES[index]) {
              const confirmBox =
                TIMES[index] === 25
                  ? window.confirm(
                      "If you finish the round early your timer will be lost and the current pomodoro will not be validated."
                    )
                  : window.confirm(
                      "If you finish the round early your timer will be lost."
                    );

              if (confirmBox === true) {
                setRunning(false);
                updateIndex(lastIndex ? 0 : index + 1);
                updateTime(lastIndex ? TIMES[0] * 60 : TIMES[index + 1] * 60);
              }
            } else if (!running || parseInt(minutes) !== TIMES[index]) {
              setRunning(false);
              updateIndex(lastIndex ? 0 : index + 1);
              updateTime(lastIndex ? TIMES[0] * 60 : TIMES[index + 1] * 60);
            }
          }}
          onMouseOver={() => {
            setNextHovered(true);
          }}
          onMouseOut={() => {
            setNextHovered(false);
          }}
        >
          <svg
            width="30"
            height="38"
            viewBox="0 0 21 22"
            className="fill-white"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M1.12982 19.6046V2.25197C1.12982 1.46762 1.99163 0.988599 2.65776 1.40268L16.6153 10.079C17.2447 10.4703 17.2447 11.3863 16.6153 11.7776L2.65776 20.4539C1.99163 20.868 1.12982 20.3889 1.12982 19.6046Z"
              fill="inherit"
            />
            <path
              d="M17.9815 20.7759L17.9815 1.08075C17.9815 0.528465 18.4292 0.0807495 18.9815 0.0807495L19.1076 0.0807495C19.6599 0.0807494 20.1076 0.528465 20.1076 1.08075L20.1076 20.7759C20.1076 21.3282 19.6599 21.7759 19.1076 21.7759L18.9815 21.7759C18.4292 21.7759 17.9815 21.3282 17.9815 20.7759Z"
              fill="inherit"
            />
          </svg>
        </button>
      </div>
      {displayMessage ? (
        <Message
          TIMES={TIMES}
          index={index}
          setDisplayMessage={setDisplayMessage}
        />
      ) : null}
    </section>
  );
}

export default Timer;
