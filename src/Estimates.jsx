import { useState, useEffect } from "react";

function Estimates({ TIMES, index, display }) {
  const timeNow = new Date();

  // N° OF POMODOROS ESTIMATION GIVEN END TIME
  const [userInputHour, setUserInputHour] = useState(timeNow.getHours());
  const [userInputMinutes, setUserInputMinutes] = useState(
    timeNow.getMinutes()
  );
  const finishTime = new Date();
  finishTime.setHours(userInputHour);
  finishTime.setMinutes(userInputMinutes);
  const availableTime = (finishTime - timeNow) / 1000 / 60; // In minutes
  const nbLongBreak = Math.floor(availableTime / (5 * 30));

  const [availablePomodoro, setAvailablePomodoro] = useState(
    Math.floor((availableTime - nbLongBreak * 10) / 30)
  );

  useEffect(() => {
    setAvailablePomodoro(Math.floor((availableTime - nbLongBreak * 10) / 30));
  }, [userInputHour, userInputMinutes]);

  if (!display) return null;
  return (
    <div className=" bg-white/20 flex flex-col ml-10 px-10 py-6 rounded-md">
      <p>End of session time:</p>
      <div>
        <input
          type="number"
          defaultValue={userInputHour}
          min={0}
          max={23}
          onChange={(e) => setUserInputHour(e.target.valueAsNumber)}
          className={`${
            TIMES[index] === 5
              ? "bg-shortbreakmd"
              : TIMES[index] === 15
              ? "bg-longbreakmd"
              : "bg-pomodoromd"
          } w-14 my-4 mr-2 text-white p-2 rounded-md outline-none appearance-none invalid:outline-white `}
        />
        :
        <input
          type="number"
          defaultValue={userInputMinutes}
          min={0}
          max={59}
          onChange={(e) => setUserInputMinutes(e.target.valueAsNumber)}
          className={`${
            TIMES[index] === 5
              ? "bg-shortbreakmd"
              : TIMES[index] === 15
              ? "bg-longbreakmd"
              : "bg-pomodoromd"
          } w-14 ml-2 text-white p-2 rounded-md outline-none appearance-none invalid:outline-white `}
        />
      </div>
      <p className="text-center mb-4"> = </p>
      <p className="text-center ">
        <span className="underline ">{availablePomodoro}</span> Pomodoro
      </p>
    </div>
  );
}

export default Estimates;
