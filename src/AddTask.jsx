import { useState, useEffect } from "react";

function AddTask({ TIMES, index, tasksRef, setTasks }) {
  // ADD NEW TASK
  const [newTask, setNewTask] = useState({});

  const handleChange = (e) => {
    const name = e.target.name;
    const value = isNaN(e.target.valueAsNumber)
      ? e.target.value
      : e.target.valueAsNumber;
    setNewTask((values) => ({ ...values, [name]: value }));
  };
  const addTask = (e) => {
    e.preventDefault();
    setTasks((tasks) => [
      ...tasks,
      {
        id: Math.random(),
        name: newTask.name,
        current: false,
        nbCurrent: 0,
        nbTotal: newTask.nbTotal || 1,
      },
    ]);
    setNewTask({});
  };

  useEffect(() => {
    const scrollToLast = (ref) =>
      ref.current.scrollTo(0, tasksRef.current.scrollHeight);
    scrollToLast(tasksRef);
  }, [newTask]);

  return (
    <form
      id="add_task"
      className="flex flex-col mt-auto lg:flex-row"
      onSubmit={addTask}
    >
      <label htmlFor="new_task" form="add_task">
        <input
          id="new_task"
          required
          type="text"
          name="name"
          value={newTask.name || ""}
          minLength={1}
          onChange={handleChange}
          placeholder="Task Name"
          className={`${
            TIMES[index] === 5
              ? "bg-shortbreakmd"
              : TIMES[index] === 15
              ? "bg-longbreakmd"
              : "bg-pomodoromd"
          }  p-2  rounded-md placeholder-white/50 focus:placeholder-transparent outline-none focus:invalid:outline-white`}
        />
      </label>
      <div className="flex justify-between items-center mt-4 ml-0 lg:ml-4 lg:mt-0">
        <div>
          <label htmlFor="pomodoro_nb" form="add_task">
            Pomodoros:
          </label>
          <input
            id="pomodoro_nb"
            required
            type="number"
            name="nbTotal"
            placeholder={1}
            value={newTask.nbTotal || ""}
            min={1}
            onChange={handleChange}
            className={`${
              TIMES[index] === 5
                ? "bg-shortbreakmd"
                : TIMES[index] === 15
                ? "bg-longbreakmd"
                : "bg-pomodoromd"
            } w-14 ml-2 placeholder-white/50 text-white p-2 rounded-md outline-none focus:invalid:outline-white `}
          />
        </div>
        <button
          type="submit"
          className={`${
            TIMES[index] === 5
              ? "text-shortbreakdark"
              : TIMES[index] === 15
              ? "text-longbreakdark"
              : "text-pomodorodark"
          } bg-white text-sm xxs:text-base p-2 rounded-md md:ml-4 shadow-md hover:shadow-xl`}
        >
          Add Task
        </button>
      </div>
    </form>
  );
}

export default AddTask;
