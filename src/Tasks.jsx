import { useState, useEffect, useRef } from "react";

import Info from "./Info";
import AddTask from "./AddTask";
import TasksList from "./TasksList";

function Tasks({ TIMES, time, running, tasks, setTasks, index }) {
  const [displayMenu, setDisplayMenu] = useState(false);

  // SET WHAT TASK IS THE CURRENT TASK
  const [unfinishedTasks, setUnfinishedTasks] = useState(
    tasks.filter((task) => task.nbCurrent < task.nbTotal)
  );
  useEffect(() => {
    setUnfinishedTasks(tasks.filter((task) => task.nbCurrent < task.nbTotal));
    const indexCurrent = tasks.findIndex((task) => task.current === true);
    let newArray = [...tasks];

    // No task is current, make first unfinished task to be current if there is
    if (indexCurrent < 0) {
      const newCurrentId = unfinishedTasks[0]?.id;
      newArray.forEach((task) => {
        if (task.id === newCurrentId) {
          task.current = true;
        }
      });
    }

    // Update current when task position changed (1rst unfinished task to be current)
    if (
      indexCurrent >= 0 &&
      unfinishedTasks[0] &&
      tasks[indexCurrent].id !== unfinishedTasks[0].id
    ) {
      const newCurrentId = unfinishedTasks[0]?.id;
      newArray.forEach((task) => {
        if (task.id === newCurrentId) {
          task.current = true;
        } else {
          task.current = false;
        }
      });
    }

    // Update current (natural flow)
    if (time > 0 && running) {
      //if current task has all its pomodoros completed make next one as current
      if (indexCurrent >= 0) {
        if (
          TIMES[index] === 25 &&
          tasks[indexCurrent].nbCurrent == tasks[indexCurrent].nbTotal
        ) {
          newArray[indexCurrent].current = false;
          if (indexCurrent + 1 < tasks.length) {
            newArray[indexCurrent + 1].current = true;
          }
          setTasks(newArray);
          newArray = [...tasks];
        }
      }
    } else if (time === 0) {
      //Timer finished, make next task to be the new current or increment current task if pomodoros not finished
      if (indexCurrent >= 0) {
        if (
          TIMES[index] === 25 &&
          indexCurrent < tasks.length &&
          newArray[indexCurrent].nbCurrent === newArray[indexCurrent].nbTotal
        ) {
          newArray[indexCurrent].current = false;
          if (indexCurrent + 1 < tasks.length) {
            newArray[indexCurrent + 1].current = true;
          }
        } else if (
          TIMES[index] === 25 &&
          indexCurrent < tasks.length &&
          newArray[indexCurrent].nbCurrent < newArray[indexCurrent].nbTotal
        ) {
          newArray[indexCurrent].nbCurrent++;
        }
      }
      setTasks(newArray);
    }
  }, [time, running, tasks]);

  // SCROLL TO CURRENT TASK
  const [userOnTasks, setUserOnTasks] = useState(false);
  const tasksRef = useRef(null);
  const indexCurrent = tasks.findIndex((task) => task.current === true);
  useEffect(() => {
    if (indexCurrent >= 0 && tasksRef.current && !userOnTasks && !displayMenu) {
      const firstElementDistance = tasksRef.current.children[0].offsetTop;
      const currentTaskDistance =
        tasksRef.current.children[indexCurrent].offsetTop;
      const scrollToCurrent = (ref) =>
        ref.current.scrollTo(
          0,
          currentTaskDistance - firstElementDistance,
          "smooth"
        );
      scrollToCurrent(tasksRef);
    }
  }, [userOnTasks]);

  // RESET DATA
  const resetData = () => {
    console.log("RD");
    localStorage.setItem(
      "tasks",
      JSON.stringify([
        {
          id: 0,
          name: "Your first task",
          current: false,
          nbCurrent: 0,
          nbTotal: 1,
        },
      ])
    );

    localStorage.setItem("index", "0");
    localStorage.setItem("time", TIMES[localStorage.getItem("index")] * 60);
    localStorage.setItem("running", false);
    window.location.reload();
  };

  return (
    <section
      className={`bg-white/10 flex flex-col my-10 mx-0  lg:my-4 min-h-[20rem] lg:min-h-[29rem] lg:max-h-[27rem] lg:ml-10 px-10 py-6 rounded-md w-full sm:w-fit sm:min-w-[63.5%] lg:min-w-fit 2xl:ml-16`}
      onMouseOver={() => setUserOnTasks(true)}
      onMouseLeave={() => setUserOnTasks(false)}
    >
      <div className="flex flex-col sm:flex-row">
        <h1 className="mb-3 text-xl font-bold">TASKS</h1>
        <Info TIMES={TIMES} index={index} time={time} tasks={tasks} />
        <button
          className={
            "mb-3 text-xs underline sm:ml-auto w-fit text-white/60 hover:text-white/80"
          }
          type="button"
          onClick={resetData}
        >
          Reset Data
        </button>
      </div>

      <hr className="border-white/50" />

      <TasksList
        TIMES={TIMES}
        tasks={tasks}
        index={index}
        tasksRef={tasksRef}
        setTasks={setTasks}
        displayMenu={displayMenu}
        setDisplayMenu={setDisplayMenu}
      />
      <AddTask
        TIMES={TIMES}
        index={index}
        tasksRef={tasksRef}
        setTasks={setTasks}
      />
    </section>
  );
}

export default Tasks;
