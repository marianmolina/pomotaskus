import { useState, useEffect } from "react";

import ModifyTask from "./ModifyTask";

function TasksList({
  TIMES,
  tasks,
  setTasks,
  index,
  tasksRef,
  displayMenu,
  setDisplayMenu,
}) {
  const [taskToModify, setTaskToModify] = useState();

  const moveUp = (e) => {
    const indexModified = tasks.findIndex(
      (task) => task.id == e.currentTarget.id.split("up")[0]
    );
    const movedTask = tasks[indexModified];
    const begArray = tasks.filter((task) => task.id !== movedTask.id);
    const arrowNb = indexModified > 0 ? indexModified - 1 : indexModified;
    const endArray = begArray.splice(arrowNb, tasks.length - arrowNb);
    begArray.push(movedTask);
    const newArray = [...begArray, ...endArray];
    setTasks(newArray);
  };
  const moveDown = (e) => {
    const indexModified = tasks.findIndex(
      (task) => task.id == e.currentTarget.id.split("down")[0]
    );
    const movedTask = tasks[indexModified];
    const begArray = tasks.filter((task) => task.id !== movedTask.id);
    const arrowNb =
      indexModified < tasks.length - 1 ? indexModified + 1 : indexModified;
    const endArray = begArray.splice(arrowNb, tasks.length - arrowNb);
    begArray.push(movedTask);
    const newArray = [...begArray, ...endArray];
    setTasks(newArray);
  };

  // CLOSE MODIFY MENU WHEN CLICK OUT OF ELEMENT
  const clickOutside = (ref) => {
    useEffect(() => {
      function handleOutsideClick(e) {
        if (ref.current && !ref.current.contains(e.target)) {
          if (e.target.id !== "modifyButton") {
            setDisplayMenu(false);
          }
        }
      }
      document.addEventListener("click", handleOutsideClick);
      return () => document.removeEventListener("click", handleOutsideClick);
    }, [ref]);
  };

  // DELETE TASK
  const deleteTask = (id) => {
    !displayMenu ? setTasks(tasks.filter((task) => task.id !== id)) : null;
  };

  return (
    <ul
      ref={tasksRef}
      className={` ${
        TIMES[index] === 5
          ? "text-shortbreakdark fill-shortbreakdark stroke-shortbreakdark border-shortbreakmd"
          : TIMES[index] === 15
          ? "text-longbreakdark fill-longbreakdark stroke-longbreakdark border-longbreakmd"
          : "text-pomodorodark fill-pomodorodark stroke-pomodorodark border-pomodorodark/75"
      } my-7 overflow-hidden hover:overflow-y-auto `}
    >
      {tasks.map((task) => (
        <li
          key={task.id}
          className={`${
            task.current
              ? "bg-black/60 font-semibold text-white fill-white stroke-white border-white/50"
              : "bg-white/40 border-inherit"
          }  p-2 mb-2 flex rounded-md`}
        >
          <div className="flex flex-col mr-3 ml-1">
            <button
              id={task.id + "up"}
              className="mb-1"
              title={`Move task ${task.name} up`}
              onClick={(e) => {
                moveUp(e);
              }}
            >
              <svg
                width="10"
                height="10"
                viewBox="0 0 14 13"
                fill="inherit"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M6.65781 1.22131C6.85026 0.887979 7.33138 0.88798 7.52383 1.22131L13.586 11.7213C13.7785 12.0546 13.5379 12.4713 13.153 12.4713H1.02864C0.643742 12.4713 0.40318 12.0546 0.59563 11.7213L6.65781 1.22131Z"
                  fill="inherit"
                />
              </svg>
            </button>
            <button
              id={task.id + "down"}
              title={`Move task "${task.name}" down`}
              onClick={(e) => {
                moveDown(e);
              }}
            >
              <svg
                width="10"
                height="10"
                viewBox="0 0 14 13"
                fill="inherit"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M7.52383 11.9607C7.33138 12.294 6.85026 12.294 6.65781 11.9607L0.59563 1.46069C0.40318 1.12736 0.643743 0.710692 1.02864 0.710692L13.153 0.710693C13.5379 0.710693 13.7785 1.12736 13.586 1.46069L7.52383 11.9607Z"
                  fill="inherit"
                />
              </svg>
            </button>
          </div>
          <div className="flex justify-end mr-5 w-3/4">
            <p
              title={task.name}
              className="truncate max-w-[5rem] xxxs:max-w-[7rem] xxs:max-w-[12rem] xs:max-w-[18rem] sm:max-w-[24rem] md:max-w-md"
            >
              {task.name}
            </p>
            <p className="ml-auto">
              {task.nbCurrent}/<span className="text-sm">{task.nbTotal}</span>
            </p>
          </div>
          <button
            id={`modifyButton`}
            title={`Modify this task: ${task.name}`}
            onClick={() => {
              setDisplayMenu(true);
              setTaskToModify(task.id);
            }}
            className="hover:bg-[rgba(255,255,255,0.38)] ml-auto px-1 rounded-md text-xs underline mr-2 border border-inherit"
          >
            <svg
              width="14"
              height="15"
              viewBox="0 0 14 49"
              fill="inherit"
              stroke="inherit"
              xmlns="http://www.w3.org/2000/svg"
              className="pointer-events-none"
            >
              <circle cx="6.84434" cy="7.32893" r="5" fill="inherit" />
              <circle cx="6.84434" cy="42.4535" r="5" fill="inherit" />
              <circle cx="6.84434" cy="24.8912" r="5" fill="inherit" />
            </svg>
          </button>

          <button
            title={`Delete this task : ${task.name}`}
            onClick={() => deleteTask(task.id)}
            className="hover:bg-[rgba(255,255,255,0.38)] px-1 rounded-md text-xs font-bold"
          >
            <svg
              width="25"
              height="25"
              viewBox="0 0 50 49"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M33.2242 16.7452L25.0782 24.8912M16.9322 33.0373L25.0782 24.8912M25.0782 24.8912L33.2797 32.9812M25.0782 24.8912L16.8765 16.8011"
                stroke="inherit"
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </button>
        </li>
      ))}
      <ModifyTask
        display={displayMenu}
        setDisplay={setDisplayMenu}
        taskId={taskToModify}
        tasks={tasks}
        setTasks={setTasks}
        clickOutside={clickOutside}
      />
    </ul>
  );
}

export default TasksList;
