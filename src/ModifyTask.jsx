import { useState, useRef, useEffect } from "react";

function ModifyTask({
  display,
  setDisplay,
  taskId,
  tasks,
  setTasks,
  clickOutside,
}) {
  const indexModified = tasks.findIndex((task) => task.id === taskId);
  const [newName, setNewName] = useState();
  const [newNbTotal, setNewNbTotal] = useState();
  const [newNbCurrent, setNewNbCurrent] = useState();

  const ref = useRef(null);
  clickOutside(ref);

  const updateTask = (e) => {
    e.preventDefault();
    setTasks((tasks) => {
      return tasks.map((task, index) => {
        if (index === indexModified) {
          return {
            ...task,
            name: newName ? newName : task.name,
            nbCurrent: newNbCurrent ? newNbCurrent : task.nbCurrent,
            nbTotal: newNbTotal ? newNbTotal : task.nbTotal,
          };
        } else {
          return task;
        }
      });
    });
    setNewName();
    setNewNbCurrent();
    setNewNbTotal();
    setDisplay(false);
  };

  useEffect(() => {
    setNewName();
    setNewNbCurrent();
    setNewNbTotal();
  }, [display]);

  const changeOrder = (e) => {
    const movedTask = tasks[indexModified];
    const begArray = tasks.filter((task) => task.id !== movedTask.id);
    const endArray = begArray.splice(
      e.target.valueAsNumber - 1,
      tasks.length - e.target.valueAsNumber + 1
    );
    begArray.push(movedTask);
    const newArray = [...begArray, ...endArray];
    setTasks(newArray);
  };

  if (!display) return null;
  return (
    <div
      className="flex absolute right-1 bottom-20 flex-col p-6 ml-8 text-black rounded-md sm:right-24 sm:bottom-24 lg:bottom-auto lg:top-32 lg:right-4 bg-slate-200"
      ref={ref}
    >
      <button
        title="Close the menu"
        onClick={() => setDisplay(false)}
        className="ml-auto mb-4 hover:bg-[rgba(255,255,255,0.38)]  px-2 rounded-md "
        autoFocus
      >
        <svg
          width="25"
          height="25"
          viewBox="0 0 50 49"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M33.2242 16.7452L25.0782 24.8912M16.9322 33.0373L25.0782 24.8912M25.0782 24.8912L33.2797 32.9812M25.0782 24.8912L16.8765 16.8011"
            stroke="black"
            strokeWidth="3"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </button>
      <form id="update_task" onSubmit={updateTask} className="flex flex-col">
        <div>
          <label htmlFor="index" form="update_task">
            Task order:{" "}
          </label>
          <input
            id="index"
            required
            type="number"
            name="index"
            min="1"
            max={tasks.length}
            defaultValue={indexModified + 1}
            onChange={changeOrder}
            className="p-2 mb-4 ml-4 w-14 rounded-md outline-none invalid:outline-slate-400"
          />
        </div>
        <div>
          <label htmlFor="name" form="update_task">
            Task name:{" "}
          </label>
          <input
            id="name"
            required
            type="text"
            name="name"
            maxLength={38}
            minLength={1}
            defaultValue={tasks[indexModified].name}
            onChange={(e) => {
              setNewName(e.target.value);
            }}
            className="p-2 mb-4 ml-4 rounded-md outline-none invalid:outline-slate-400"
          />
        </div>
        <div>
          <label htmlFor="current_nb total_nb" form="update_task">
            Pomodoros:{" "}
          </label>
          <input
            title="Current Number"
            id="current_nb"
            required
            type="number"
            name="nbCurrent"
            defaultValue={tasks[indexModified].nbCurrent}
            min={0}
            max={newNbTotal ? newNbTotal : tasks[indexModified].nbTotal}
            onChange={(e) => {
              setNewNbCurrent(
                e.target.value === "0" ? e.target.value : e.target.valueAsNumber
              );
            }}
            className="p-2 ml-4 w-14 rounded-md outline-none invalid:outline-slate-400"
          ></input>{" "}
          /{" "}
          <input
            title="Total Number"
            id="total_nb"
            required
            type="number"
            name="nbTotal"
            min={1}
            defaultValue={tasks[indexModified].nbTotal}
            onChange={(e) => {
              setNewNbTotal(e.target.valueAsNumber);
            }}
            className="p-2 w-14 rounded-md outline-none invalid:outline-white"
          />
        </div>
        <button
          type="submit"
          className="p-2 mt-6 ml-auto bg-white rounded-md shadow-md hover:shadow-xl"
        >
          Save
        </button>
      </form>
    </div>
  );
}

export default ModifyTask;
