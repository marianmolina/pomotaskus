import { useState, useEffect } from "react";

const DATA = [
  { id: 1, name: "Prepare yourself a nice drink", status: false },
  { id: 2, name: "Do some stretching", status: false },
  { id: 3, name: "Any dishes to be washed?", status: false },
  { id: 4, name: "Check if the plants need to be watered", status: false },
  { id: 6, name: "Pick up the laundry", status: false },
  { id: 7, name: "Walk around the home/garden", status: false },
];

if (!localStorage.getItem("activities")) {
  localStorage.setItem("activities", JSON.stringify(DATA));
}

function Message({ TIMES, index, setDisplayMessage }) {
  const [activities, setActivities] = useState(
    JSON.parse(localStorage.getItem("activities"))
  );

  useEffect(() => {
    localStorage.setItem("activities", JSON.stringify(activities));
  }, [activities]);

  // CHECK ACTIVITY
  const changeStatus = (e) => {
    const checkbox = e.currentTarget;
    const activityID = checkbox.id;
    checkbox.checked
      ? (activities[activityID].status = true)
      : (activities[activityID].status = false);
    localStorage.setItem("activities", JSON.stringify(activities));
  };

  // DELETE ACTIVITY
  const deleteActivity = (id) => {
    setActivities(activities.filter((activity) => activity.id !== id));
  };

  // ADD ACTIVITY
  const [newActivity, setNewActivity] = useState({});
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    console.log(e.target.value);

    setNewActivity({ [name]: value });
  };
  const addActivity = (e) => {
    e.preventDefault();
    setActivities((activities) => [
      ...activities,
      {
        id: Math.random(),
        name: newActivity.name,
        status: false,
      },
    ]);
    console.log(newActivity);
    setNewActivity({});
  };

  return (
    <div className="flex absolute top-0 left-0 justify-center items-center w-screen h-screen">
      <div className="flex flex-col justify-between p-8 bg-white rounded-md shadow-xl w-fit xs:w-5/6 h-fit lg:w-fit text-slate-800">
        <div className="flex flex-wrap-reverse mb-4">
          <h1 className="text-2xl font-black text-center">
            Let's get up from your seat and get active for{" "}
            {TIMES[index] !== 25 ? TIMES[index] : TIMES[index - 1]} minutes? ♥
          </h1>
        </div>
        <div>
          <div className="flex flex-col items-center mt-4 mb-8 xs:justify-between xs:flex-row">
            <h2 className="mb-2 text-lg underline xs:mb-0">Some ideas:</h2>
            <div className="flex justify-center w-fit">
              <form
                id="add_activity"
                className="flex fill-gray-400 hover:fill-slate-800 focus:fill-slate-800"
                onSubmit={addActivity}
              >
                <input
                  id="activity_name"
                  type="text"
                  name="name"
                  minLength={1}
                  onChange={handleChange}
                  value={newActivity.name || ""}
                  placeholder="Activity Name"
                  className="p-1 pl-2 mr-4 ml-2 placeholder-gray-400 bg-gray-100 rounded-md border border-gray-400 outline-none stext-sm focus:placeholder-transparent focus:invalid:outline-slate-700 hover:border-slate-700"
                  required
                />
                <button type="submit" title="Add new activity">
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 49 49"
                    className="fill-inherit hover:fill-inherit"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <circle
                      cx="24.1869"
                      cy="24.8912"
                      r="24.1088"
                      fill="inherit"
                    />
                    <path
                      d="M24.187 13.371V24.8912M24.187 36.4114V24.8912M24.187 24.8912L35.7068 24.8124M24.187 24.8912L12.6669 24.9701"
                      stroke="white"
                      strokeWidth="4"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </button>
              </form>
            </div>
          </div>
          <ul className="flex flex-col text-sm lg:flex-wrap lg:h-36">
            {activities.map((activity, i) => (
              <li key={i} className="flex items-center mr-4 mb-2">
                <button
                  title="Delete this activity"
                  onClick={() => deleteActivity(activity.id)}
                  className="px-1 mr-1 text-xs font-bold text-slate-800 hover:text-red-600"
                >
                  <svg
                    width="20"
                    height="20"
                    viewBox="0 0 50 49"
                    className="stroke-slate-800 hover:stroke-red-600"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M33.2242 16.7452L25.0782 24.8912M16.9322 33.0373L25.0782 24.8912M25.0782 24.8912L33.2797 32.9812M25.0782 24.8912L16.8765 16.8011"
                      stroke="inherit"
                      strokeWidth="3"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </button>
                <input
                  type="checkbox"
                  role="checkbox"
                  aria-checked={activity.status ? true : false}
                  id={i}
                  onChange={(e) => changeStatus(e)}
                  defaultChecked={activity.status ? true : false}
                  className="mr-2 w-3 h-3 bg-gray-200 rounded-sm appearance-none cursor-pointer checked:bg-slate-800"
                />
                {activity.name}
              </li>
            ))}
          </ul>
        </div>
        <hr className="my-4 mt-8 border-slate-400"></hr>
        <div className="text-xs text-gray-600">
          <p>
            The{" "}
            <a
              href="https://www.gov.uk/government/publications/physical-activity-guidelines-uk-chief-medical-officers-report"
              className="underline"
            >
              UK Chief Medical Officers' Physical Activity Guidelines report
            </a>{" "}
            recommends breaking up long periods of sitting time with activity
            for just 1 to 2 minutes.{" "}
          </p>
          <p className="my-2">
            Studies have linked being inactive with being overweight and obese,
            type 2 diabetes, some types of cancer, and early death.
          </p>
          <p>
            Sitting for long periods is thought to slow the metabolism, which
            affects the body's ability to regulate blood sugar, blood pressure
            and break down body fat.
          </p>
          <p className="mt-2 text-gray-400">
            More info on{" "}
            <a
              className="underline hover:text-gray-800"
              href="https://www.nhs.uk/live-well/exercise/exercise-guidelines/why-sitting-too-much-is-bad-for-us/"
            >
              NHS Website
            </a>{" "}
            and{" "}
            <a
              className="underline hover:text-gray-800"
              href="https://www.who.int/publications/i/item/9789240015128"
            >
              WHO Guidelines on physical activity and sedentary behaviour
            </a>
          </p>
        </div>
        <hr className="my-2 mt-4 border-slate-400"></hr>

        <button
          autoFocus
          title="Close this window"
          onClick={() => setDisplayMessage(false)}
          className="flex justify-center mt-4 mr-auto ml-auto w-2/3 bg-gray-200 rounded-md focus:outline-slate-800 hover:bg-gray-300 lg:w-1/3"
        >
          Got it!
        </button>
      </div>
    </div>
  );
}

export default Message;
