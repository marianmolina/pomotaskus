import { useState, useEffect } from "react";

import { useWakeLock } from "react-screen-wake-lock";

import Timer from "./Timer";
import Tasks from "./Tasks";

const TIMES = [25, 5, 25, 5, 25, 5, 25, 5, 25, 30];

if (!localStorage.getItem("tasks")) {
  localStorage.setItem(
    "tasks",
    JSON.stringify([
      {
        id: 0,
        name: "Your first task",
        current: false,
        nbCurrent: 0,
        nbTotal: 1,
      },
    ])
  );
}
if (!localStorage.getItem("index")) {
  localStorage.setItem("index", "0");
}
if (!localStorage.getItem("time")) {
  localStorage.setItem("time", TIMES[localStorage.getItem("index")] * 60);
}
if (!localStorage.getItem("running")) {
  localStorage.setItem("running", false);
}

function App() {
  // SET UP APP DATA
  const [index, updateIndex] = useState(
    parseInt(localStorage.getItem("index"))
  );
  const [time, updateTime] = useState(parseInt(localStorage.getItem("time")));
  const [running, setRunning] = useState(eval(localStorage.getItem("running"))); //i.e. timer is not running
  const [tasks, setTasks] = useState(JSON.parse(localStorage.getItem("tasks")));
  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);
  useEffect(() => {
    localStorage.setItem("index", index);
  }, [index]);
  useEffect(() => {
    localStorage.setItem("time", time);
  }, [time]);
  useEffect(() => {
    localStorage.setItem("running", running);
  }, [running]);

  // SET UP SCREEN WAKE LOCK
  const { isSupported, released, request, release } = useWakeLock({
    onRequest: () => alert("Screen Wake Lock: requested!"),
    onError: () => alert("An error happened 💥"),
    onRelease: () => alert("Screen Wake Lock: released!"),
  });

  return (
    <div
      className={`${
        TIMES[index] === 5
          ? "bg-shortbreak"
          : TIMES[index] === 30
          ? "bg-longbreak"
          : "bg-pomodoro"
      }  flex flex-col min-h-screen text-white cursor-default`}
    >
      {isSupported ? (
        <button
          className={`${
            released === false ? "text-white/40" : "text-white/80 "
          } py-2 px-5 ml-auto mr-4 mt-4 text-xs  rounded-md w-fit border border-white/40 hover:bg-black/10`}
          type="button"
          onClick={() => (released === false ? release() : request())}
        >
          {released === false
            ? "Allow screen to lock"
            : "Prevent screen from locking"}
        </button>
      ) : null}
      <main
        className={
          "flex flex-col justify-center items-center mt-auto lg:flex-row 2xl:py-20"
        }
      >
        <Timer
          TIMES={TIMES}
          time={time}
          updateTime={updateTime}
          running={running}
          setRunning={setRunning}
          tasks={tasks}
          index={index}
          updateIndex={updateIndex}
        />
        <Tasks
          TIMES={TIMES}
          time={time}
          running={running}
          tasks={tasks}
          setTasks={setTasks}
          index={index}
        />
      </main>
      <footer
        className={
          "flex flex-col items-center mt-auto mb-5 text-xs font-light text-black/30"
        }
      >
        <a
          href="https://gitlab.com/marianmolina/pomotaskus"
          target="_blank"
          className="flex mb-2 hover:text-white/50 fill-black/30 hover:fill-white/50"
        >
          <svg
            width="17"
            height="15.5"
            fill="inherit"
            viewBox="0 0 143 132"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M142.4 74.3L134.8 51L119.7 4.69997C118.9 2.29997 116.6 0.599976 114 0.599976C111.4 0.599976 109.2 2.19997 108.4 4.69997L94.1001 48.7H49.0001L34.7001 4.69997C33.9001 2.19997 31.7001 0.599976 29.1001 0.599976C26.5001 0.599976 24.3001 2.19997 23.4001 4.69997L8.40012 51L0.800121 74.3C-0.299879 77.8 0.90012 81.7 3.90012 83.8L69.6001 131.5C69.6001 131.5 69.7001 131.5 69.7001 131.6C69.7001 131.6 69.6001 131.6 69.6001 131.5C69.7001 131.6 69.8001 131.6 69.9001 131.7C70.0001 131.7 70.0001 131.7 70.1001 131.8C70.2001 131.8 70.2001 131.8 70.3001 131.9C70.4001 131.9 70.5001 131.9 70.5001 132H70.6001C70.6001 132 70.6001 132 70.7001 132C70.9001 132 71.0001 132 71.2001 132C71.4001 132 71.5001 132 71.7001 132C71.7001 132 71.7001 132 71.8001 132H71.9001C72.0001 132 72.1001 132 72.1001 131.9C72.2001 131.9 72.2001 131.9 72.3001 131.8C72.4001 131.8 72.4001 131.8 72.5001 131.7C72.6001 131.6 72.7001 131.6 72.8001 131.5L138.5 83.8C142.3 81.7 143.6 77.8 142.4 74.3ZM114.1 8.89996L127 48.6H101.2L114.1 8.89996ZM124.8 55.3L119.5 62L80.7001 111.7L99.0001 55.3H124.8ZM68.4001 129.9C68.5001 129.9 68.5001 129.9 68.4001 129.9C68.5001 129.9 68.5001 129.9 68.4001 129.9ZM62.6001 111.8L18.6001 55.4H44.3001L62.6001 111.8ZM29.2001 8.89996L42.1001 48.6H16.3001L29.2001 8.89996ZM7.90012 78.4C7.30012 77.9 7.00012 77.1 7.20012 76.4L12.9001 59L54.3001 112.1L7.90012 78.4ZM69.6001 131.5C69.5001 131.5 69.5001 131.5 69.6001 131.5C69.5001 131.4 69.5001 131.4 69.6001 131.5C69.5001 131.4 69.5001 131.4 69.5001 131.4L69.4001 131.3C69.4001 131.4 69.5001 131.4 69.6001 131.5ZM71.6001 118L60.8001 84.8L51.2001 55.3H92.0001L71.6001 118ZM73.8001 131.4C73.7001 131.5 73.7001 131.5 73.7001 131.5C73.7001 131.5 73.6001 131.5 73.6001 131.6C73.7001 131.5 73.8001 131.4 73.9001 131.3C73.9001 131.3 73.9001 131.3 73.8001 131.4ZM135.4 78.4L89.0001 112.1L130.5 59L136.1 76.4C136.3 77.1 136 77.9 135.4 78.4Z"
              fill="inherit"
            />
          </svg>
          <p className="ml-2 underline">View the code on Gitlab</p>
        </a>
        <div className="flex flex-wrap justify-center">
          <p className="mr-1 mb-1 lg:mb-0">©2022 Marian Molina.</p>
          <p>
            <a
              rel="license"
              href="http://creativecommons.org/licenses/by/4.0/"
              target="_blank"
              className="underline decoration-black/30 hover:decoration-white/50 hover:text-white/50"
            >
              Creative Commons Attribution 4.0 International License
            </a>
            .
          </p>
        </div>
      </footer>
    </div>
  );
}

export default App;
